geoMath = {        
    // 0 degree is heading nord for example: (x=0, y=1)        
    polarToVector: function(d, angle){      
        var a = angle;  
        var y = d * Math.cos(
            geoMath.round(geoMath.degreeToRadians(a), 3)                
        );
        var x = d * Math.sin(
            geoMath.round(geoMath.degreeToRadians(a), 3) 
        );
        return {
            x: x,
            y: y
        };        
    },
    vectorToPolar: function (x, y){           
        var a = geoMath.radiansToDegree(Math.atan2(x, y));            
        a+=360;
        a=a%360;
        return {
            d: Math.sqrt(x*x+y*y),
            angle: a
        };        
    },
    degreeToRadians: function (angle) {
        return angle * (Math.PI / 180);
    },
    radiansToDegree: function (rad) {
        return rad * (180 / Math.PI);
    },
    round: function (number, digits){
        var i = Math.pow(10,digits);        
        return Math.round(number * i) / i;        
    },
    distance : function (x1, y1, x2, y2){
        return Math.sqrt(
            (x1 - x2) * (x1 - x2) +
            (y1 - y2) * (y1 - y2)
        );        
    },
    generateUniqueRandomNumbers: function(c,max){
        var ret = [];
        while(ret.length < c){
          var randomnumber=Math.floor(Math.random()*max);
          var found=false;
          for(var i=0;i<ret.length;i++){
                if(ret[i]==randomnumber){found=true;break}
          }
          if(!found)ret[ret.length]=randomnumber;
        }
        return ret;
    }
} 
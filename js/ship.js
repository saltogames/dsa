var shipIdAutoInc = 0;

var Ship = function (typeId, x, y, angle, swarm) {    
    shipIdAutoInc++;
    this.id = shipIdAutoInc;            
    var t = parameters.shipTypes.getById(typeId);
    
    // PARAMETERS       
    this.max_speed = t.maxSpeed;    // px/s
    this.max_health = t.health; 
    this.typeName = t.name;
    
    // CONSTANTS
    this.w = parameters.ship.w;
    this.h = parameters.ship.h;    
    this.typeId = typeId;
    this.type = t;
    
    // VARIABLES 
    this.swarm = swarm; // backward reference
    this.x = x;
    this.y = y;
    this.angle = angle;
    this.speed = 0;
    this.health = this.max_health;
    
    // ADD GUN    
    this.gun = new Gun(this, this.type.gun);
        
    
    switch (swarm.owner){
    	case 'player':
    		screen.doScreenFlash('white');
    		break;
    	case 'computer':
    		break;
    }
};

// FUNCTIONS USED BY THE SCHEDULER        
Ship.prototype.step = function(framerate){       
    if (this.health < 1) {
        this.die();
        return;
    }    
    this.applyRules();        
    this.applyTypeDependentRules();
    this.float(framerate);    
    if (this.swarm.isNearTarget) this.gun.stop();
    else {if (!this.gun.on) this.gun.start();}    
    screen.draw(this);
    this.gun.step();
};
 
Ship.prototype.applyRules = function(){                
    // TARGET? 
    var dx = 0;
    var dy = 0;            
    // IF ANY TARGET: HEAD IT WITH FULL SPEED     
    if (this.swarm.moveTarget !== null){
        if ( Math.abs(this.swarm.moveTarget.y - this.y) + Math.abs(this.swarm.moveTarget.x - this.x) > 25){                  
            var toTargetX = this.swarm.moveTarget.x - this.x;
            var toTargetY = this.swarm.moveTarget.y - this.y;                        
            var polar = geoMath.vectorToPolar(toTargetX, toTargetY);                        
            var v = geoMath.polarToVector(this.max_speed, polar.angle); 
            dx += v.x;
            dy += v.y;
        }       
    }

    // ALL OTHER MOVEMENT VECTORS
    var cv = this.swarm.getCollectiveVector(this);
    dx += cv.x;
    dy += cv.y;


    // EASING    
    var easingScale = 1;
    if (this.swarm.isNearTarget) easingScale = 0.15;
    if ((dx !== 0) && (dy !== 0)){                
        var thisD = geoMath.polarToVector(this.speed, this.angle);                
        dx = dx * parameters.swarm.takeActionEasing * easingScale + thisD.x * (1-parameters.swarm.takeActionEasing);
        dy = dy * parameters.swarm.takeActionEasing * easingScale + thisD.y * (1-parameters.swarm.takeActionEasing);                                 
    }        
    

    // SET    
    var p = geoMath.vectorToPolar(dx, dy);    
    if (p.angle !== null) this.angle = p.angle;    
    this.speed = p.d;                           
    
};


Ship.prototype.applyTypeDependentRules = function(){
    if (this.typeName === 'Angel'){
        // heal ally with lowest hp
        if (world.tick % (world.framerate * this.type.special.healCooldown) === 0) {
            var min = 9999;
            var minShip = null;
            for (var sid in this.swarm.ships){ 
                var s = this.swarm.ships[sid];   
                if (s.health < min && s.health < s.max_health) {
                    min = s.health;
                    minShip = s;
                }
            }
            if (minShip != null) {
                minShip.health = Math.min(minShip.max_health, minShip.health + this.type.special.healAmount);
                world.advanced.addFlittingAnim(minShip, 16, 17, 'healing', 'heal.png');                                
            }
        }  
        
    }
    if (this.typeName === 'Trobadour'){
        // console.log("IM FUCKING HERE ");
    }
};
    
Ship.prototype.float = function (framerate){
    
    if (this.speed > 0) {        
        var v = geoMath.polarToVector(this.speed / framerate, this.angle);    
        this.x += v.x;
        this.y += v.y;
    }       
};

Ship.prototype.die = function (){
    this.gun.stop();
    delete this.swarm.ships[this.id]; 
    screen.clear(this);
    world.advanced.addExplosion(this.x,this.y); 
    if (this.swarm.owner == 'computer') {
        screen.doScreenFlash('red');
        world.doCameraShake();
    }
};
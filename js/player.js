player = {
    pressedKeys : new Set(),
    mouseLastX: 0,
    mouseLastY: 0,
    keyPressed : (keyCode) => {      
        if (player.pressedKeys.has(keyCode)) return;
        player.pressedKeys.add(keyCode);    
    },
    keyReleased : (keyCode) => {               
        player.pressedKeys.delete(keyCode);
    },    
    applyNewTarget : () => {
        let dx = 0;
        let dy = 0;
        userSwarms = world.getPlayers();  
        let keys = player.pressedKeys;
        let length = Array.from(keys).length;
        let sqrt2 = Math.SQRT2;
        // 38 == w == up
        if (keys.has(38) && length === 1){
            dy = 160;
        };    
        // 40 == s == down
        if (keys.has(40) && length === 1){
            dy = -160;
        };
        // 37 == a == left
        if (keys.has(37) && length === 1){
            dx = -160;
        };    
        // 39 == d == right 
        if (keys.has(39) && length === 1){
            dx = 160;
        };
        // up and left
        if (keys.has(38) && keys.has(37) && length === 2){
            dy = 160 * sqrt2;
            dx = - 160 * sqrt2;
        }; 
        // up and right
        if (keys.has(38) && keys.has(39) && length === 2){
            dy = 160 * sqrt2;
            dx = 160 * sqrt2;
        }; 
        // down and left
        if (keys.has(40) && keys.has(37) && length === 2){
            dy = - 160 * sqrt2;
            dx = - 160 * sqrt2;
        }; 
        // down and right
        if (keys.has(40) && keys.has(39) && length === 2){
            dy = -160 * sqrt2;
            dx = 160 * sqrt2;
        }; 
        for (let i = 0; i < userSwarms.length; ++i) {
            let usw = userSwarms[i];      
            userSwarms[i].newTarget(usw.x + dx, usw.y + dy);
        }
    },
    mouseClicked : (mx, my) => {      
        player.mouseLastX = mx;
        player.mouseLastY = my;
    },
    mouseMoved : (mx, my) => {
        player.mouseLastX = mx;
        player.mouseLastY = my;
    },
}

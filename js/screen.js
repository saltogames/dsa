screen = {  
    w: 0,
    h: 0,
    
    // JQuery object to the different draw layers    
    screenJQ : null, 
    minimapJQ : null,
    latticeJQ : null,
    paralax1JQ : null,
    paralax2JQ : null,        
    init : function (screenJQ, latticeJQ, paralax1JQ, paralax2JQ, screenFlashLayerJQ){
        screen.screenJQ = screenJQ;
        screen.latticeJQ = latticeJQ;
        screen.paralax1JQ = paralax1JQ;
        screen.paralax2JQ = paralax2JQ;
        screen.screenFlashLayerJQ = screenFlashLayerJQ;
        screen.w = screenJQ.width() - 50;
        screen.h = screenJQ.height() - 50;
        screenJQ.append($("<div id='horisontal_worldend_layer' style='display:none'></div>"));
        screenJQ.append($("<div id='vertical_worldend_layer' style='display:none'></div>"));
        screen.minimapJQ = $(screenJQ.find('#minimap'));      
        screen.minimapBckJQ = $(screenJQ.find('#minimap_bck'));   
    },
    
    /* 
     * obj is insance of Ship, Bullet, Meteor, Explosion or Swarm
     * obj should have: id,x,y,w,h,angle
     * obj meay have: health, maxHealth
     */
    getPrefix : function (obj){   
        if (obj instanceof Ship) return 'ship'; 
        else if (obj instanceof Bullet) return 'bullet';
        else if (obj instanceof Meteor) return 'meteor'; 
        else if (obj instanceof Swarm) return 'swarm';
        else if (obj instanceof Animation) return 'animation';
        else if (obj instanceof FlittingAnim) return 'flittinganim';
        else if (obj instanceof ShipBubble) return 'shipbubble';
        else return null;
    },
    draw : function (obj){                        
        // DETECT TYPE
        var typePrefix = screen.getPrefix(obj);        
        if (typePrefix == null) return; // unsupported type         
 
        // ADD ANY ELEMENT TO SCREEN IF
        // - WITHIN BOUNDS
        // - DOM ELEMENT NOT NOT EXISTS YET AND
        // - IS NOT DEAD  
        var objJQ = $("#" + typePrefix + "_" + obj.id);
        var hbRedJQ = 1;
        var hbGreenJQ = 1;
        var c = typePrefix;
        var hbc = '';      
        var objW = 0;
        var objH = 0;                        
        if (typePrefix == 'swarm') c+= ' centerpoint'; 
        if (typePrefix == 'meteor') hbc+= 'meteor';  
        if (
                screen.isWithinBounds(obj) &&
                objJQ.length == 0 &&
                (typeof(obj.health) === 'undefined' || obj.health > 0)
            ){                     
            var objJQ = $("<div id='" + typePrefix + "_" + obj.id + "' class='obj " + c + "'></div>");                        
            if (typePrefix == 'ship'){                
                var type = parameters.shipTypes.getById(obj.typeId); 
                objJQ.css('background', 'url(pics/ships/s/spaceship0' + (obj.typeId+1) + '_' + obj.swarm.color + '.png)');
                objW = type.w
                objH = type.h;                
            } else if (typePrefix === 'shipbubble'){
                // console.log(obj.shipId);
                var type = parameters.shipTypes.getById(obj.shipId);  
                var f = "url('pics/ships/s/spaceship0" + (obj.shipId+1) + "_" + world.playerSwarm.color + ".png')";                
                objJQ.css('background', f);                                
                objW = type.w;
                objH = type.h;
            } else if (typePrefix === 'bullet'){
                var type = parameters.shipTypes.getById(obj.ship.typeId);        
                objJQ.css('background', 'url(' + type.gun.bullet.url + ')');
                objW = type.gun.bullet.w
                objH = type.gun.bullet.h;
            } else if (typePrefix === 'animation'){   
                console.log('add animation');
                objJQ.css('background', 'url(' + obj.spriteUrl + ')');
                objJQ.addClass(obj.name);
                objW = obj.w;
                objH = obj.h;
            } else if (typePrefix === 'flittinganim'){    
                objJQ.css('background', 'url(pics/flitting/' + obj.picFileName + ')');
                objJQ.addClass(obj.name);
                objW = obj.w;
                objH = obj.h;
            } else {
                objW = obj.w;
                objH = obj.h;
            }                         
            objJQ.width(objW);
            objJQ.height(objH);
            screen.screenJQ.append(objJQ);
            if (typeof(obj.health) !== 'undefined'){                
                hbRedJQ = screen.screenJQ.append($("<div id='" + typePrefix + "_" + obj.id + "_hbgreen' class='healthbar green " + hbc + "' " + c + "></div>"));
                hbGreenJQ = screen.screenJQ.append($("<div id='" + typePrefix + "_" + obj.id + "_hbred' class='healthbar red " + hbc + "'></div>"));
                screen.screenJQ.append(hbRedJQ);
                screen.screenJQ.append(hbGreenJQ);
            }                   
        } else {
            // ALREADY A DOM ELEMENT
            objW = objJQ.width();
            objH = objJQ.height();
            
        }
                
        // DEL IF
        // - DOM EXISTS AND
        // - OUT OF BOUNDS
        if (/*$("#" + typePrefix + "_" + obj.id).lenght && */!screen.isWithinBounds(obj)) {
            screen.clear(obj);
        }
                
        // TUNE DRAW PARAMTERS        
        var co = world.getCameraOffset();   
        var top = screen.screenJQ.height() - (obj.y + objH / 2) + co.y;        
        var left = obj.x - objW / 2 + co.x;                           
        objJQ.css("top", top + "px");
        objJQ.css("left", left + "px");
        screen.rotateDom(objJQ, obj.angle);           
        if (typePrefix == 'shipbubble') {
            objJQ.css('background', "url('pics/ships/s/spaceship0" + (obj.shipId+1) + "_" + world.playerSwarm.color + ".png')");             
        }
        if (typePrefix == 'ship') {
            var type = parameters.shipTypes.getById(obj.typeId);  
            var i = obj.gun.shipAnimationFrameInd;
            if (i == 0) {
                objJQ.css('background-position', '0px');            
            } else if (i == 1) {
                objJQ.css('background-position', (-1 *  type.w) + 'px');            
            } else if (i == 2) {
                objJQ.css('background-position', (-2 *  type.w) + 'px');            
            }                       
        }        
        
        if (typeof(obj.health) !== 'undefined'){        
            hbRedJQ = $("#" + typePrefix + "_" + obj.id+ "_hbred");
            hbGreenJQ = $("#" + typePrefix + "_" + obj.id+ "_hbgreen");
            hbRedJQ.css("top", (top - objH / 4) + "px");    
            hbRedJQ.css("left", (left - 6) + "px");    
            hbGreenJQ.css("top", (top - objH / 4 + 1) + "px");    
            hbGreenJQ.css("left", (left - 6) + "px");    
            hbGreenJQ.css("width", (obj.health / obj.max_health * 40) + "px");             
            var maxHealthBarPixel = hbRedJQ.width();
            var healthBarPixel = (obj.health / obj.maxHealth) * maxHealthBarPixel;
            hbGreenJQ.css("width", parseInt(healthBarPixel) + "px");            
        }             
    }, 
    drawRayBeam : function(rayGun){        
        var s = rayGun.ship;
        var t = rayGun.target;
        if (t === null) return;
        var id = rayGun.gun.id;
        var objJQ = $("#raybeam_" + id);                
        var polar = geoMath.vectorToPolar(t.x - s.x, t.y - s.y)        
        var objH = polar.d;
        var objW = 3;  
        if (
                (screen.isWithinBounds(s) || screen.isWithinBounds(t))
                && objJQ.length === 0
        ){            
            // ADD IF NOT ALREADY ON SCREEN
            objJQ = $("<div id='raybeam_" + id + "' class='obj raybeam'></div>");                         
            screen.rotateDom(objJQ, polar.angle);
            screen.screenJQ.append(objJQ);                                                                      
        }
        
        // DEL IF
        // - OUT OF BOUNDS
        // - TURNED OFF
        if (
            (!screen.isWithinBounds(s) && !screen.isWithinBounds(t))
            || !rayGun.onTarget
           )
        {
            $("#raybeam_" + id).remove();           
        }
        
         // TUNE DRAW PARAMTERS        
        var co = world.getCameraOffset();
        var cx = (s.x + t.x) / 2;
        var cy = (s.y + t.y) / 2;
        var top = screen.screenJQ.height() - (cy + objH / 2) + co.y;        
        var left = cx + co.x;                           
        objJQ.css("top", top + "px");
        objJQ.css("left", left + "px");    
        var o = 0.3 + (rayGun.tck % 5) * 0.1;
        objJQ.css('opacity', o);
        objJQ.css('background', rayGun.ship.swarm.color);
        screen.rotateDom(objJQ, polar.angle);           
        objJQ.width(objW);
        objJQ.height(objH);                    
    },
    deleteRayBeam : function(rayGun){
        var id = rayGun.gun.id;
        var objJQ = $("#raybeam_" + id);
        objJQ.remove();
    },
    minimapFullMeteorSight : false, // true, if there is a Trobadour in army
    drawMini : function(obj){                
        var typePrefix = screen.getPrefix(obj);        
        if (typePrefix == null) return; // unsupported type
        var miniObjJQ = $("#mini_" + typePrefix + "_" + obj.id);        
        if (miniObjJQ.length == 0 && (typePrefix == 'swarm' || typePrefix == 'meteor') &&
           (typeof(obj.health) == 'undefined' || obj.health > 0)){
            var miniObjJQ = $("<div id='mini_" + typePrefix + "_" + obj.id + "' class='" + typePrefix + " mini' style='background: " + obj.color + "'></div>");
            screen.minimapJQ.append(miniObjJQ);
            if (typePrefix == 'swarm' && obj.owner == "player"){
                var miniSightObjJQ = $("<div id='mini_swarm_sightrange_" + obj.id + "' class='swarm sightrange mini'></div>");
                var r = obj.sightRadius * world.minimapScale;
                miniSightObjJQ.width(r * 2);
                miniSightObjJQ.height(r * 2);
                miniSightObjJQ.css('border-radius', r + 'px');
                screen.minimapJQ.append(miniSightObjJQ);
            }
        }
        if (typePrefix == 'meteor' || typePrefix == 'swarm'){
            miniObjJQ.css("bottom", obj.y * world.minimapScale + "px");
            miniObjJQ.css("left", obj.x * world.minimapScale + "px");
        }
        if (typePrefix == 'swarm' && obj.owner == "player") {
            // draw self swarm
            var miniSightObjJQ =  $("#mini_swarm_sightrange_" + obj.id);
            var r = obj.sightRadius * world.minimapScale;
            miniSightObjJQ.width(r * 2);
            miniSightObjJQ.height(r * 2);
            miniSightObjJQ.css('border-radius', r + 'px');
            miniSightObjJQ.css("bottom", obj.y * world.minimapScale - r + "px");
            miniSightObjJQ.css("left", obj.x * world.minimapScale - r + "px");
        }
        
    	// sight on minimap
        swarms = world.getPlayers();
        for (let i in swarms) {
            var swarm = swarms[i];
            var c = swarm.getCenter();        	
            if (
                    (geoMath.distance(obj.x, obj.y, c.x, c.y) < swarm.sightRadius) ||
                    (screen.minimapFullMeteorSight && typePrefix === 'meteor') // ||
                    // typePrefix === 'swarm'
            ) {
                    miniObjJQ.css('display', 'block');                        
            } else {
                miniObjJQ.css('display', 'none');
            }
        }
    },
    drawBounds: function(){
        var c = world.camera;
        var hwelJQ = $("#horisontal_worldend_layer");
        var vwelJQ = $("#vertical_worldend_layer");
        if (c.y - screen.h / 2 < 0) {
            var h = - (c.y - screen.h / 2);
            hwelJQ.css('top', (screen.h - h) + 'px'); 
            hwelJQ.css('height', h + 'px');
            hwelJQ.css('width', screen.w + 'px');
            hwelJQ.css('display','block');
        } else if (c.y + screen.h / 2 > world.h){
            var h = c.y + screen.h / 2 - world.h;
            hwelJQ.css('top', '0px'); 
            hwelJQ.css('height', h + 'px');
            hwelJQ.css('width', screen.w + 'px');
            hwelJQ.css('display','block');
        } else {
            hwelJQ.css('display','none');
        }
        if (c.x - screen.w / 2 < 0) {
            var w = - (c.x - screen.w / 2);
            vwelJQ.css('left', '0px'); 
            vwelJQ.css('height', screen.h);
            vwelJQ.css('width', w + 'px');
            vwelJQ.css('display','block');
        } else if (c.x + screen.w / 2 > world.w){
            var w = c.x + screen.w / 2 - world.w;
            vwelJQ.css('left', (screen.w - w) + 'px'); 
            vwelJQ.css('height', screen.h + 'px');
            vwelJQ.css('width', w + 'px');
            vwelJQ.css('display','block');
        } else {
            vwelJQ.css('display','none');
        }
    },
    clear : function (obj){
        var typePrefix = screen.getPrefix(obj);        
        if (typePrefix == null) return; // unsupported type
        var objJQ = $("#" + typePrefix + "_" + obj.id); 
        
        if (objJQ.length){
            objJQ.remove();
            if (typeof(obj.health) !== 'undefined'){
                $("#" + typePrefix + "_" + obj.id+ "_hbred").remove();
                $("#" + typePrefix + "_" + obj.id+ "_hbgreen").remove();
            }
        }        
    },
    clearMini : function (obj){
        var typePrefix = screen.getPrefix(obj);        
        if (typePrefix == null) return; // unsupported type
        var miniObjJQ = $("#mini_" + typePrefix + "_" + obj.id);
        if (miniObjJQ.length){
            miniObjJQ.remove();
        }        
    },
    isWithinBounds : function (obj) {
        var co = world.getCameraOffset();
        var xx = obj.x + co.x;
        var yy = obj.y - co.y;
        return xx > 0 && xx < screen.w && yy > 0 && yy < screen.h;
    },
    moveLattice : function(){        
        var wco = world.getCameraOffset();
        screen.latticeJQ.css('background-position-x', wco.x + 'px');    
        screen.latticeJQ.css('background-position-y', wco.y + 'px');
        
        $('#horisontal_worldend_layer').css('background-position-x', wco.x + 'px');    
        $('#horisontal_worldend_layer').css('background-position-y', wco.y + 'px');
        $('#vertical_worldend_layer').css('background-position-x', wco.x + 'px');    
        $('#vertical_worldend_layer').css('background-position-y', wco.y + 'px');        
        
        //screen.latticeJQ.css('background-position', wco.x + 'px ' + wco.y + 'px'); // mozilla
    },
    paralaxIdCounter : 0,
    paralax : {
        addObjAtRandomPlace : function (layerID){                          
            var id = screen.paralaxIdCounter;
            screen.paralaxIdCounter++;
            var typeID = parseInt(Math.random()*2)+1;            
            var parObjJQ = $("<div id='parobj_lay" + layerID + "_id_" + id + "' class='paralax_obj layer" + layerID + " type" + typeID + "'>");                
            var rotateAngle = parseInt(Math.random()*366);
            //screen.rotateDom(parObjJQ, rotateAngle);
            parObjJQ.width(parameters.paralax.objw);
            parObjJQ.height(parameters.paralax.objh);                
            var p = null;
            if (layerID == 1){
                var s = parameters.paralax.layer1Scale;
                var p = {
                    startX : (Math.random() * (screen.w/s*s)),
                    startY : (Math.random() * (screen.h/s*s*2))
                }  
            } else if (layerID == 2){
                var s = parameters.paralax.layer2Scale;
                var p = {
                    startX :  (Math.random() * (screen.w/s*s)),
                    startY :  (Math.random() * (screen.h/s*s))
                } 
            }    
            parObjJQ.css('left', p.startX + 'px');
            parObjJQ.css('bottom', p.startY + 'px');
            if (layerID == 1) {
                screen.paralax.layer1World[id] = p;
                screen.paralax1JQ.append(parObjJQ);
            } else if (layerID == 2) {
                screen.paralax.layer2World[id] = p;
                screen.paralax2JQ.append(parObjJQ);
            }                                             
        },
        layer1World : {},
        layer2World : {},
        init : function() {
            for (var i=0; i<parameters.paralax.layer1ObjCount; i++)
                screen.paralax.addObjAtRandomPlace(1);  
            for (var i=0; i<parameters.paralax.layer2ObjCount; i++)
                screen.paralax.addObjAtRandomPlace(2);
        },
        moveParalax : function() {
            var wc = world.camera;               
            var s1 = parameters.paralax.layer1Scale;
            for (var id in screen.paralax.layer1World){
                var o = screen.paralax.layer1World[id];                 
                var domObj = $('#parobj_lay1_id_' + id);                            
                domObj.css('left', (o.startX - wc.x / s1) + 'px');
                domObj.css('bottom', (o.startY - wc.y / s1) + 'px');
            }
            var s2 = parameters.paralax.layer2Scale;
            for (var id in screen.paralax.layer2World){
                var o = screen.paralax.layer2World[id];                 
                var domObj = $('#parobj_lay2_id_' + id);                            
                domObj.css('left', (o.startX - wc.x / s2) + 'px');
                domObj.css('bottom', (o.startY - wc.y / s2) + 'px');
            }
        }
    },
    rotateDom : function (domObjJQ, angle){
        var rotateString = "rotate(" + angle + "deg)";        
        domObjJQ.css("-ms-transform", rotateString);
        domObjJQ.css("-webkit-transform", rotateString); 
        domObjJQ.css("transform", rotateString); 
    },
    doScreenFlash : function (color){
        screen.screenFlashLayerJQ.css('background', color);
        screen.screenFlashLayerJQ.css('opacity', 0.35);
        screen.screenFlashLayerJQ.animate({
            opacity: 0,            
        }, 130);        
        
    },    
}
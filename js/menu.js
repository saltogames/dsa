menu = {
    startMenu : {
        selectedRotationAngle : 0,
        selectedSlotId : -1,
        selectedSlotTimer : -1,
        color : null,
        show : function (color, startClickedCallback) {
            this.color = color;
            $('#startmenu .topbordercorner').css('display', 'none');      
            $('#startmenu .topborder').css('display', 'none');      
            $('#startmenu .namecontainer').css('display', 'none');      
            $('#startmenu .topcontainer').css('display', 'none');      
            $('#startmenu .shipbrickcontainer').css('display', 'none');      
            $('#startmenu').css('display', 'block');     
            $('#startmenu .middlecontainer').css('margin-top', '195px');      

            // centerize menu
            var ww = window.innerWidth;
            var mw = $('#startmenu').width();
            $('#startmenu').css('left', (ww / 2 - mw / 2) + 'px');      

            // make selected circles rotate        
            menu.startMenu.selectedSlotTimer = setInterval(function(){            
                menu.startMenu.selectedRotationAngle+=1;
                if (menu.startMenu.selectedRotationAngle > 360) menu.startMenu.selectedRotationAngle = menu.startMenu.selectedRotationAngle%360;
                var rotateString = "rotate(" + menu.startMenu.selectedRotationAngle + "deg)";        
                var domObjJQ = $('#startmenu .shipcircle.selected');           
                domObjJQ.css("-ms-transform", rotateString);
                domObjJQ.css("-webkit-transform", rotateString); 
                domObjJQ.css("transform", rotateString);                     
            },50);

            // fill shipbricks with all ships
            for (var i=0; i < parameters.shipNames.length; i++){
                // var typeInfo = parameters.ship.types[i];
                var url = 'pics/ships/s/spaceship0' + (i+1) + '_' + color + '.png';
                $('#startmenu .shipbrick' + i + ' .pic').css('background-image', 'url('+ url +')');   
                
            }

            // click on main slots
            $('#startmenu .shipcircle').click(function(){            
                $('#startmenu .shipbrickcontainer').css('display', 'block');                  
                var id = $(this).attr('id').slice(10);
                var sscJQ = $('#startmenu .shipcircle.selected');            
                if (id == 0) sscJQ.css('left', '102px');
                if (id == 1) sscJQ.css('left', '279px');
                if (id == 2) sscJQ.css('left', '454px');
                sscJQ.css('display', 'block');
                
                // reset click label on all if necessery
                for (var i = 0; i < 3; i++) {                    
                    var jq = $('#shipcirclepic' + i)
                    var attr = jq.attr('src');
                    if (typeof attr === typeof undefined || attr === false) {                                     
                        $('#shipcirclepic' + i).css('display', 'none');
                        $('#shipcircleclick' + i).css('display', 'block');
                    }
                }
                
                //$('#shipcirclepic' + id).css('display', 'block');  
                $('#shipcircleclick' + id).css('display', 'none');  
                var attr = $('#shipcirclepic' + id).attr('src');
                if (typeof attr !== typeof undefined && attr !== false) {                      
                    var shipTypeId = parseInt($('#shipcirclepic' + id).attr('src').slice(15,18));                                                                                                         
                    menu.startMenu.fillTopContent(parameters.shipTypes.getById(id));                                        
                }
                menu.startMenu.selectedSlotId = id;
            });

            // click on shipbricks
            $('#startmenu .shipbrick').click(function(){           
                // show top menu
                $('#startmenu .middlecontainer').css('margin-top', '0px');    

                // fill ship circles and top menu
                var id = parseInt($(this).attr('id').slice(2));    
                menu.startMenu.showTopMenu();
                menu.startMenu.fillTopContent(parameters.shipTypes.getById(id));
                menu.startMenu.changeActiveShipCirclePic(id);                                              
            });
            
            // click on fight
            $('#startmenu .startbutton-cont').click(function(){                 
                startClickedCallback(
                    parseInt($('#shipcirclepic0').attr('src').slice(13,15))-1,
                    parseInt($('#shipcirclepic1').attr('src').slice(13,15))-1,
                    parseInt($('#shipcirclepic2').attr('src').slice(13,15))-1
                );
            });
            
        },
        showTopMenu : function(){
            $('#startmenu .topcontainer').css('display', 'block');  
            $('#startmenu .topbordercorner').css('display', 'block');      
            $('#startmenu .topborder').css('display', 'block');      
            $('#startmenu .namecontainer').css('display', 'block');                                                
        },
        hideTopMenu : function (){
            $('#startmenu .topbordercorner').css('display', 'none');      
            $('#startmenu .topborder').css('display', 'none');      
            $('#startmenu .namecontainer').css('display', 'none');      
            $('#startmenu .topcontainer').css('display', 'none');       
        },
        fillTopContent : function(shipType) {            
            // fill
            $('#startmenu .name').html(shipType.name);
            $('#startmenu .description').html(shipType.desc);
            // stat bricks     
            $('#startmenu .brick').css('display','none');
            $('#startmenu .darkbrick').css('display','block');                        
            menu.startMenu.fillStatBricks(shipType.stats);                                               
            // centerize
            var tcw = $('#startmenu .topborder').width()-10;
            var tw = $('#startmenu .name').width();                     
            $('#startmenu .line').width(tcw / 2 - tw / 2 - 1);
        },
        fillStatBricks : function(stats){            
            for (var i=0; i<stats.length; i++){
                if (stats[i] > 0) {                
                    $('#startmenu .stat' + (i+1) + ' .brick.brick1').css('display','block');
                    $('#startmenu .stat' + (i+1) + ' .darkbrick.brick1').css('display','none');
                }
                if (stats[i] > 1){                
                    $('#startmenu .stat' + (i+1) + ' .brick.brick2').css('display','block');
                    $('#startmenu .stat' + (i+1) + ' .darkbrick.brick2').css('display','none');
                }
                if (stats[i] > 2){                
                    $('#startmenu .stat' + (i+1) + ' .brick.brick3').css('display','block');
                    $('#startmenu .stat' + (i+1) + ' .darkbrick.brick3').css('display','none');
                }
            }
        },
        changeActiveShipCirclePic : function(id) {             
            // $('#startmenu .shipcirclepic').css('display', 'none');
            $('#shipcirclepic' + menu.startMenu.selectedSlotId).css('display', 'inline');
            $('#shipcirclepic' + menu.startMenu.selectedSlotId).attr('src', 'pics/ships/l/0' + (id+1) + '_' + this.color + '.png');                       
            if (
                $('#shipcirclepic0').css('display') === 'inline' &&
                $('#shipcirclepic1').css('display') === 'inline' &&
                $('#shipcirclepic2').css('display') === 'inline'
            ) {                
                $('#startmenu .startbutton-cont').css('display', 'block');
            }
        },
        hide : function (){
            $('#startmenu').css('display', 'none'); 
        }
    }    
}
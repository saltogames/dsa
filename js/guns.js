/* THE GUN CLASS GENERALIZES OTHER GUNS.*///////////////////////////////////////
var gunId = 0;
var Gun = function (ship, gunParameters){
    this.id = gunId;
    gunId++;
    if (gunParameters == null) {
        this.g = new NoGun();
        return;
    }
    this.isOn = false;       
    if (gunParameters.damageTransmitter == 'bullet')
        this.g = new BulletGun (ship, gunParameters.cooldown, gunParameters.bullet, this)
    else if (gunParameters.damageTransmitter == 'ray')
        this.g = new RayGun(ship, gunParameters.ray, this)    
    else 
        this.g = new NoGun();
        
        
}
Gun.prototype.step =  function(){
    if (!this.isOn) return;
    this.g.step();      
};
Gun.prototype.start = function(){           
    this.isOn = true;
    this.g.start();
};
Gun.prototype.stop =  function(){        
    this.isOn = false;           
    this.g.stop();
};
/* NO GUN*/
var NoGun = function(){    
}
NoGun.prototype.step = function(){               
};
NoGun.prototype.start = function(){               
};
NoGun.prototype.stop =  function(){                  
};

/*REGULAR (BULLET-EMITING) GUNS*////////////////////////////////////////////////
var BulletGun = function (ship, cooldown, bulletParameters, gun) {       
    this.ship = ship;
    this.bp = bulletParameters;    
    this.cooldown = cooldown;
    this.isOn = false;
    this.tck = 0;
    this.shipAnimationFrameInd = 0;
    this.gun = gun; // kind of a parent    
};
BulletGun.prototype.step =  function(){         
    this.tck++;            
    if (this.tck % (world.framerate * this.cooldown) == 1) {
        let mySwarm = this.ship.swarm;
        // FIRE                        
        if (this.ship.swarm.owner === "computer"){                        
            let tx = mySwarm.moveTarget.x;
            let ty = mySwarm.moveTarget.y;            
            let sx = mySwarm.x;            
            let sy = mySwarm.y;
            let vx = tx - sx;
            let vy = -(ty - sy);
            let polar = geoMath.vectorToPolar(vx, vy);                  
            var b = new Bullet(this.ship, this.bp, polar.angle);
        } else {
            let mx = player.mouseLastX;
            let my = player.mouseLastY;
            var smx = screen.w / 2;
            var smy = screen.h / 2;
            let vx = mx - smx;
            let vy = -(my - smy);
            let polar = geoMath.vectorToPolar(vx, vy);                  
            var b = new Bullet(this.ship, this.bp, polar.angle);
        }
            
        this.ship.swarm.bullets[b.id] = b;      
        this.shipAnimationFrameInd = 1;
    }                                    
    if (this.tck % (world.framerate * this.cooldown) == 3) {
        this.shipAnimationFrameInd = 2;
    }
    if (this.tck % (world.framerate * this.cooldown) == 5) {
        this.shipAnimationFrameInd = 0;
    }       
};
BulletGun.prototype.start = function(){               
};
BulletGun.prototype.stop =  function(){         
    this.tick = 0;
    this.shipAnimationFrameInd = 0;    
};

/////////////*RAY GUNS (THEY LOCK TARGETS AND DO CONSTANT DAMAGE OVER TIME)*////////////////
var RayGun = function (ship, rayParameters, gun) {       
    this.ship = ship;        
    this.rp = rayParameters;
    this.onTarget = false;
    this.tck = 0;
    this.dps = rayParameters.dps;
    this.distance = rayParameters.maxTargetDistance;
    this.target = null;    
    this.targetType = null;    
    this.gun = gun; // kind of a parent
};
RayGun.prototype.start = function (){    
    //world.activeRayGuns[this.gun.id];
    this.isOn = true;
}
RayGun.prototype.step =  function(){                
    if (this.onTarget && this.target != null) {
        // DAMAGE IF THERE IS A TARGET
        this.tck++;        
        var damage = this.dps / world.framerate;
        if (this.targetType == 'meteor') damage *= 2;
        this.target.health -= damage;
        if (this.target.health < 0) this.stop();
        screen.drawRayBeam(this);
    } else {        
        // IF NO TARGET: CHECK FOR TARGETS
        var minD = this.distance+1;
        // 1) ships. if more are within borders, get the closest        
        var targetS = null;
        for (var i =0; i < world.swarms.length; i++){           
            var sw = world.swarms[i];
                        
            if (sw.id === this.ship.swarm.id) continue;
            for (var sid in sw.ships){ 
                var s = sw.ships[sid];
                var d = geoMath.distance(this.ship.x, this.ship.y, s.x, s.y);
                if (d < this.distance && d < minD){
                    // theres a target                
                    minD = d;
                    targetS = s;
                }  
            }
                     
        }
        if (targetS === null){
            // no target ship in range
            // 2) meteors. if more are within borders, get the closest        
            var targetM = null;
            for (var mid in world.meteors){ 
                var m = world.meteors[mid];
                var d = geoMath.distance(this.ship.x, this.ship.y, m.x, m.y);
                if (d < this.distance && d < minD){
                    // theres a target                
                    minD = d;
                    targetM = m;
                }            
            }
            if (targetM === null){
                // no target in range                
                this.stop();            
            } else {
                // there's a target
                this.setTarget(targetM, 'meteor');
            }           
        } else {
            // there's a target
            this.setTarget(targetS, 'ship');
        }
        
    }
};
RayGun.prototype.setTarget = function(target, ttype){           
    this.target = target;
    this.targetType = ttype;
    this.onTarget = true;
};
RayGun.prototype.stop =  function(){  
    this.target = null;
    this.targetType = null;
    this.isOn = false;     
    this.onTarget = false;
    this.tck = 0;
    screen.deleteRayBeam(this);
    //delete world.activeRayGuns[this.gun.id];    
};

var swarmIdAutoInc = 0;
var Swarm = function (owner, color, count, startPosition) {            
    swarmIdAutoInc++;
    
    // VARIABLES
    this.owner = owner;
    this.id = swarmIdAutoInc;
    this.ships = {};   // identified by id to be easily del
    this.bullets = {}; // identfied by id, to be able to easily delete
    this.moveTarget = null;
    this.isNearTarget = false;
    this.color = color;
    this.sightRadius = parameters.swarm.swarmBasicSightRadius;
    this.position = startPosition;
    this.x = 0;
    this.y = 0; // will be the centerpoint in every step        

    this.addRandomShips(count, startPosition);
    
    // set the start point to initial target. swarms love targets.
    var cp = this.getCenter();
    this.newTarget(cp.x, cp.y);
    
    // ADD CENTERPOINT DRAWING
    this.newTarget(this.getCenter().x, this.getCenter().y);

    if (owner == 'computer') {
        // Ai.add(this);
    }

    screen.screenJQ.append($("<div id='swarm_" + this.id + "' class='swarm centerpoint'></div>"));
};

Swarm.prototype.empty = function(){    
    this.ships = []; 
}
Swarm.prototype.addNewShip = function(shipTypeId, x, y){
    var s = new Ship(shipTypeId,x,y,0,this);
    this.ships[s.id] = s; 
    // console.log("En vagyok a " + shipTypeId + " es itt vagyok: " + x + "," + y);
}
Swarm.prototype.addNewShipToCenterPoint = function(shipTypeId){
    this.addNewShip(shipTypeId,this.x,this.y);
};
Swarm.prototype.addRandomShips = function(c, position){
    for (var i=0; i<c; i++){
        var d = 200;        
        var s = new Ship(
            parseInt(Math.random() * parameters.shipNames.length),              // typeId
            Math.floor(position.x + (Math.random() - 0.5) * d),                 // x
            Math.floor(position.y + (Math.random() - 0.5) * d),                 // y
            Math.floor(Math.random()*365),                                      // angle
            this                                                                // the containing swarm
        );
        this.ships[s.id] = s; 
    }       
    this.newTarget(this.x, this.y);
}; 


Swarm.prototype.step = function(){           
    // near target?
    if (this.moveTarget != null){        
        var d = geoMath.distance(this.x,this.y,this.moveTarget.x, this.moveTarget.y);
        if (d < 50)
        	this.isNearTarget = true;
        else this.isNearTarget = false;
    }    
        
    // step ships
    // - calculate centerpoint
    // - calcuclate swarm sight radius
    this.sightRadius = parameters.swarm.swarmBasicSightRadius;        
    var isThereTrobadour = false;
    for (var sid in this.ships){     
        var s = this.ships[sid];
        s.step(world.framerate);
        // if (world.advanced.outOfBounds(s.x, s.y)) s.die();            
        if (s.typeName === 'Trobadur') {
            this.sightRadius += s.type.special.sightRadiusModification;
            isThereTrobadour = true;            
        } 
    }                         
    if (isThereTrobadour && (this.id === world.getPlayers()[0].id)) {
        screen.minimapFullMeteorSight = true;        
    }            
    var cp = this.getCenter();        
    
    for (var bid in this.bullets) {
    	var bullet = this.bullets[bid];
    	bullet.step(this.framerate);
    	if (world.advanced.outOfBounds(bullet.x, bullet.y)) {
    		bullet.delete();
    	}
    }
    // move (=draw) swarm on mini screen

    this.moveCenterPointOnScreen(); 
    this.x = cp.x;
    this.y = cp.y;
    screen.drawMini(this);
};

Swarm.prototype.newTarget = function(x,y){    
    this.moveTarget = {
        x: x,
        y: y
    }    
}
Swarm.prototype.noTarget = function(){
    this.moveTarget = null;    
}

Swarm.prototype.size = function(){          
    var s = 0;
    for (var sid in this.ships) {
        if (this.ships.hasOwnProperty(sid)) s++;
    }
    return s;
};  

Swarm.prototype.getCollectiveVector = function(ship){      
    var dx = 0;
    var dy = 0;
    for (var sid in this.ships){                         
        var s = this.ships[sid];        
        if (s.id === ship.id) continue;
        
        // emergency bugfix avoid swarm collapse (when one ship is too far from centerpoint the swarm's ships kinda explode)
        var cp = this.getCenter();
        // console.log(distance(cp,s));
        // console.log(parameters.swarm.tooFarTreshold * 5);
        
        if (distance(cp, s) > parameters.swarm.tooFarTreshold * 5) {
            s.x = cp.x;
            s.y = cp.y;
            console.log('Emergency ship translatation to swarm centerpoint.. gosh..  -.-');
        }
        
        // If too close, go away
        if ((Math.abs(s.y - ship.y) + Math.abs(s.x - ship.x) < parameters.swarm.tooCloseTreshold)){             
            var ddx = (parameters.swarm.tooCloseTreshold - Math.abs(s.x - ship.x)) * parameters.swarm.tooCloseScale;
            if (s.x > ship.x) dx -= ddx;
            else dx += ddx;
            
            var ddy = (parameters.swarm.tooCloseTreshold - Math.abs(s.y - ship.y)) * parameters.swarm.tooCloseScale;
            if (s.y > ship.y) dy -= ddy;            
            else dy += ddy;
        }
        
        // If too far, go closer
        if ((Math.abs(s.y - ship.y) + Math.abs(s.x - ship.x) > parameters.swarm.tooFarTreshold)){   
            var ddx = (parameters.swarm.tooFarTreshold - Math.abs(s.x - ship.x)) * parameters.swarm.tooFarScale;
            if (s.x > ship.x) dx += ddx;
            else dx -= ddx;
            
            var ddy = (parameters.swarm.tooFarTreshold - Math.abs(s.y - ship.y)) * parameters.swarm.tooFarScale;
            if (s.x > ship.x) dy += ddy;            
            else dy -= ddy;            
        }   
        
        // Synchronize angle
        var d = Math.abs(ship.angle - s.angle);
        if (d > 360 - d){
            if (ship.angle < s.angle) ship.angle += d;
            else ship.angle -= d* (1/this.size()) * parameters.swarm.avgAngleScale;
        } else {
            d = 360 - d;
            if (ship.angle > 180) ship.angle = (ship.angle + d* (1/this.size()) * parameters.swarm.avgAngleScale) % 365;
            else ship.angle += d* (1/this.size()) * parameters.swarm.avgAngleScale;
        }
    }                           
    
    return {
        x: dx,
        y: dy
    };
};  


Swarm.prototype.getCenter = function(){   
    if (this.size() == 0) return {x:-3, y:-3};
    var x = 0;
    var y = 0;
    for (var sid in this.ships) {
        x += this.ships[sid].x;
        y += this.ships[sid].y;
    }    
    return {
        x: x / this.size(),
        y: y / this.size()
    };
};  

Swarm.prototype.moveCenterPointOnScreen = function (){                
    var co = world.getCameraOffset();
    var top = screen.screenJQ.height() - (this.y + 1) + co.y;
    var left = this.x - 1 + co.x;        
    $("#swarm_" + this.id).css("top", top + "px");
    $("#swarm_" + this.id).css("left", left + "px");
};
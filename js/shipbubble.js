var shipBubbleIdAutoInc = 0;

var ShipBubble = function (x, y) {    
    this.shipId = parseInt(Math.random() * parameters.shipNames.length);
    shipBubbleIdAutoInc++;
    this.id = shipBubbleIdAutoInc;    
    
    // CONSTANTS
    this.w = parameters.shipBubble.width;
    this.h = parameters.shipBubble.height;
    this.animFrameLength = 40; //ms
    this.animLength = 9;  // frames
    this.angle = 0;
    this.turnPeriod = parameters.shipBubble.turnPeriod;
    this.startTick = world.tick;
    
    // VARIABLES 
    this.x = x; 
    this.y = y;   
    this.s = 0;
    this.color = 'yellow';
    this.possibleShipIds = geoMath.generateUniqueRandomNumbers(3, parameters.shipNames.length);  
    this.shipIdIndex = 0;          
    this.init = true;
    
    // add the animation
    this.animation = new Animation(
        x,
        y,
        this.w,
        this.h,
        'shipbubble_border',                                 // name
        36,                                                  // time of 1 frame (ms)
        36,                                                  // frame count
        'pics/sprites/powerup_anim_szeles.png',              // url of the sprite
        true
    );
    world.animations[this.animation.id] = this.animation;
};

// FUNCTIONS USED BY THE SCHEDULER  
ShipBubble.prototype.step = function(){    
    this.rotate(); 
    screen.draw(this);
    for (var i in world.swarms)
    {
    	var d = geoMath.distance(world.swarms[i].x, world.swarms[i].y, this.x, this.y);
        if (d < this.w / 2)
            this.taken(world.swarms[i]);
    }
    if ((world.tick - this.startTick) % (world.framerate * parameters.shipBubble.changePeriod) == 0) {
		this.shipIdIndex = (this.shipIdIndex + 1) % this.possibleShipIds.length;
        this.shipId = this.possibleShipIds[this.shipIdIndex];
    }
    this.animation.x = this.x;
    this.animation.y = this.y;
    this.animation.step();
};
    
ShipBubble.prototype.rotate = function (){                    
    this.angle += (1 / this.turnPeriod) * 360 / world.framerate;        
};

ShipBubble.prototype.delete = function (){ 
    this.animation.delete();
    screen.clear(this);
    delete world.shipbubbles[this.id];           
}

ShipBubble.prototype.taken = function (swarm){
    if (swarm.size() < parameters.swarm.maxShipCount) swarm.addNewShip(this.shipId, this.x, this.y);
    if (swarm.owner === 'computer') {
    	Ai.shipBubbleTaken(swarm);
    }
    this.delete();           
}


$(document).ready(function(){    
    benchmark.create();
});

benchmark = {
    divJQ : null,
    lastMeasuredStepDuration: null,
    stepStartTime : null,    
    create: function (){
        this.divJQ = $('#benchmark');
        benchmark.update();
        setInterval(
            function () {                                          
                benchmark.update();                          
            },
            (1000)
        );
    },
    update: function (){    
        this.divJQ.empty();        
        this.divJQ.append($("<p>")
            .html('<b>Tick per sec</b>: ' + world.framerate)
        );
        this.divJQ.append($("<p>")
            .html('<b>Available time for 1 step (ms)</b>: ' + 1/world.framerate*1000 + '')
        );
        this.divJQ.append($("<p>")
            .html('<b>1 step time (ms)</b>: ' + this.lastMeasuredStepDuration)
        );
    },
    
    // CALLED FROM WORLD.STEP
    onWorldStepStart: function(){
        var d = new Date();        
        this.stepStartTime = d.getTime();
    },
    onWorldStepEnd: function(){
        var d = new Date();        
        var stepEndTime = d.getTime();
        this.lastMeasuredStepDuration = stepEndTime - this.stepStartTime;
    }
    
}
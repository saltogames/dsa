var meteorIdAutoInc = 0;

var Meteor = function (x, y) {    
    meteorIdAutoInc++;
    this.id = meteorIdAutoInc;    
    
    // CONSTANTS
    this.maxHealth = parameters.meteor.health;            
    this.w = parameters.meteor.width;
    this.h = parameters.meteor.height;             
    this.turnPeriod = parameters.meteor.turnPeriod; // secs
    this.color = parameters.meteor.color;
    
    // VARIABLES 
    this.x = x; 
    this.y = y;
    this.angle = 0;    
    this.health = this.maxHealth;    
    this.isDead = false;
          
    // drawing    
    screen.draw(this);   
    screen.drawMini(this);
};

// FUNCTIONS USED BY THE SCHEDULER        
Meteor.prototype.step = function(){ 
    if (this.health < 1) {        
        this.die();
        return;
    }
    this.rotate(); 
    screen.draw(this);   
    screen.drawMini(this);
};
    
Meteor.prototype.rotate = function (){                    
    this.angle += (1 / this.turnPeriod) * 360 / world.framerate;        
};

Meteor.prototype.delete = function (){        
    delete world.meteors[this.id]; 
}

Meteor.prototype.die = function (){        
    this.delete();       
    screen.clear(this);
    screen.clearMini(this);    
    world.advanced.addBigExplosion(this.x,this.y);    
    
    // spawn a new meteor and a ship pickup bubble
    world.advanced.addMeteor(
        Math.random() * world.w,
        Math.random() * world.h
    );
    world.advanced.addShipBubble(this.x,this.y);
}


userActionsBlocked = false;
var ual = {
    init : () => {        
        window.addEventListener('keydown', ual.keyDown,false);
        window.addEventListener('keyup', ual.keyUp,false);
        window.addEventListener("mouseclick", ual.mouseClick, false);
        window.addEventListener("mousemove", ual.mouseMove, false);
    },
    
    /* wasd means same as arrows */
    keyMtpx : (keyCode) => {           
        let kc = keyCode;                
        if (kc === 68) kc = 39;
        if (kc === 65) kc = 37;
        if (kc === 83) kc = 40;
        if (kc === 87) kc = 38;
        return kc;
    },
    
    keyDown : (e) => {                    
        if (userActionsBlocked) return;                                                                
        player.keyPressed(ual.keyMtpx(e.keyCode));
    },
    
    keyUp : (e) => {      
        if (userActionsBlocked) return;       
        player.keyReleased(ual.keyMtpx(e.keyCode));
    },
    
    
    mouseClick : (e) => {
        if (userActionsBlocked) return;       
        // send ping request to server
        var ev = window.event || e; // old IE isupport
        player.mouseClicked(ev.clientX, ev.clientY);
    }, 
    mouseMove : (e) => {
        if (userActionsBlocked) return;       
        // send ping request to server
        var ev = window.event || e; // old IE isupport
        player.mouseMoved(ev.clientX, ev.clientY);    
    },
    
    
    /* for e.g. construction */
    /* lastMouseX : 0,
    lastMouseY : 0,
    mouseMove : (e) => {
        if (!si.construction.ison || userActionsBlocked) return; // not sure                
        var e = window.event || e; // old IE support        
        ual.lastMouseX = e.clientX;
        ual.lastMouseY = e.clientY;        
        si.construction.updateFrame(ual.lastMouseX, ual.lastMouseY);        
        // todo ... 
    },
    */
    
}
var flittingAnimIdAutoInc = 0;

var FlittingAnim = function (targetObj, w, h, name, picFileName) {    
    flittingAnimIdAutoInc++;
    this.id = flittingAnimIdAutoInc;  
    
    // CONSTANTS
    this.w = w;
    this.h = h;
    this.name = name;    
    this.picFileName = picFileName;
    this.tickPerFrame = 1;
    this.frameCount = 20;
    this.pxPerFrame = 5;
    
    // VARIABLES 
    this.targetObj = targetObj;
    this.x = targetObj.x; 
    this.y = targetObj.y;         
    this.tck = 0;    
          
    this.init = true;                
};

// FUNCTIONS USED BY THE SCHEDULER  
FlittingAnim.prototype.step = function(){             
    this.tck++;            
    var maxTick = this.tickPerFrame *  this.frameCount;
    if (this.tck >= maxTick) this.delete;
    else {
        // tracking
        this.x = this.targetObj.x; 
        this.y = this.targetObj.y; 
        
        // end or step
        if (this.tck % this.tickPerFrame == 0){                                            
            $("#flittinganim_" + this.id).css("opacity", 1 - this.tck / maxTick);            
            this.y = this.targetObj.y + this.pxPerFrame * this.tck;                
            this.x = this.targetObj.x;
        }  
        screen.draw(this);
    }
};    

FlittingAnim.prototype.delete = function (){   
    screen.clear(this);
    delete world.flittingAnims[this.id];   
}


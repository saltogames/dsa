var aiIdAutoInc = 0;
Ai = {
    id: aiIdAutoInc,
    swarms: [],
    states: [],
    targets: [],
    targetTypes: [],

    add: function (pSwarm){
        this.swarms.push(pSwarm);
        this.states.push('move');
        this.targets.push(pSwarm.moveTarget);
        this.targetTypes.push('spaceLocation');
    },

    step: function () {
        for (var i in this.swarms) {
            //console.log(this.swarms[i].color + ": " + this.states[i] + "-ben vagyok es " + this.targets[i] + " a celom, ami egy " + this.targetTypes[i]);  
            //console.log(currentTarget)
            let currentAiSwarm = this.swarms[i];
            if (currentAiSwarm.x === 0) return; // the system is not set yet

            // YOYO AI: engage-escape-engage-escape
            var ad = parameters.ai.attackDistance;
            var ed = parameters.ai.escapeDistance;
            var tRange = parameters.ai.triggerRange; // pixel

            // I) CHOOSE TARGET AND MODE
            // I/1) 
            // IF ANY METEORS IN ESCAPE RANGE: ESCAPE ONE 
            // TODO: ESCAPE CLOSEST
            var b = false;
            for (var mid in world.meteors) {
                var m = world.meteors[mid];
                var p = new Position(currentAiSwarm.x, currentAiSwarm.y);
                //console.log("disztensz iz " + distance(m, p) + " " + parameters.ai.meteorEvadeDistance);
                if (distance(m, p) < parameters.ai.meteorEvadeDistance) {                    
                    this.states[i] = 'escape';
                    this.targets[i] = m;
                    this.targetTypes[i] = 'meteor';
                    b = true;
                }
            }
            if (!b) {
                // console.log("POWERUP");
                // I/2) 
                // IF ANY POWERUPS IN SIGHT RANGE - HEAD ONE
                // TODO: IF ANY POWERUPS IN SIGHTRANGE - HEAD THE CLOSEST     
                b = false;
                for (var sbid in world.shipbubbles) {
                    var sb = world.shipbubbles[sbid];
                    if (distance(sb, currentAiSwarm) < currentAiSwarm.sightRadius) {
                        this.states[i] = 'move';
                        this.targets[i] = sb;
                        this.targetTypes[i] = 'shipbubble';
                        b = true;
                    }
                }
                if (this.targetTypes[i] !== 'shipbubble') {
                    // console.log("ENGAGE");
                    // I/3) ELSE
                    //    - IF ANY SWARMS IN ATTACK RANGE ATTACK ONE       
                    //    - IF ANY SWARMS IN ESCAPE RANGE: ESCAPE ONE
                    //var d = geoMath.distance(player.x, player.y, currentSwarm.x, currentSwarm.y);
                    for (var j=0; j<world.swarms.length; j++) {
                        var targetSwarm = world.swarms[j];
                        if (this.swarms[i].id === targetSwarm.id) continue;                        
                        var d = distance(targetSwarm, currentAiSwarm);                        
                        if (d < ed + tRange / 2 && d > ed - tRange / 2) {
                            this.states[i] = 'escape';
                            this.targetTypes[i] = 'swarm';
                            this.targets[i] = targetSwarm;
                        } else if (d < ad + tRange / 2 && d > ad - tRange / 2) {
                            this.states[i] = 'engage';
                            this.targets[i] = targetSwarm;
                            this.targetTypes[i] = 'swarm'; 
                        }
                    }
                    
                    if ((this.targetTypes[i] != 'swarm')) {
                        // I/4) ELSE (type is meteor)
                        // ATTACK ANY METEROS IN IF IN ATTACK RANGE
                        var b = false;
                        for (var mid in world.meteors) {
                            var m = world.meteors[mid];
                            var d = distance(m, this.swarms[i]);
                            if (d < ad + tRange / 2 && d > ad - tRange / 2) {
                                this.states[i] = 'engage';
                                this.targetTypes[i] = 'meteor';
                                this.targets[i] = m;
                                b = true;
                            }
                        }
                        if (this.states[i] !== 'engage' && this.states[i] !== 'escape') {
                            // I/5) ELSE: HEAD RANDOM METEOR                    
                            // TODO: HEAD CLOSEST
                            var b = false;
                            for (var mid in world.meteors) {
                                var m = world.meteors[mid];
                                if (distance(m, this.swarms[i]) < this.swarms[i].sightRadius) {
                                    this.states[i] = 'move';
                                    this.targetTypes[i] = 'meteor';
                                    this.targets[i] = m;
                                    b = true;
                                }
                            }
                            
                            if (!b){
                                // 'ROAM' -> now head world center
                                this.states[i] = 'move';
                                this.targetTypes[i] = 'world_location';
                                this.targets[i] = {
                                    x: world.w / 2,
                                    y: world.h / 2
                                };
                            }
                        }
                    }
                }                        
            }
            
            
                                    
            // console.log("OTHER ACTION");
            // II) TAKE ACTION BASED ON MODE (by following or avoiding a target)        
            //     important: swarms are always in move (when there is no target, its an error)
            if (this.targets[i] == null){
                // how can we get into this state?? XD
                this.targetTypes[i] = 'none';
                this.states[i] = 'stay';
            } else {
                if (this.states[i] == 'engage' || this.states[i] == 'move') {
                    //console.log("move or engage");
                    this.swarms[i].newTarget(this.targets[i].x, this.targets[i].y);
                } else if (this.states[i] == 'escape'){
                    //console.log("escape");
                    var dx = this.swarms[i].x - this.targets[i].x;
                    var dy = this.swarms[i].y - this.targets[i].y;
                    this.swarms[i].newTarget(this.swarms[i].x + dx, this.swarms[i].y + dy);                    

                } else {
                    //console.log("mi van??");
                }
            }

        }                                                            

        // console.log(currentState + ' ' + currentTargetType);            
    },

    shipBubbleTaken: function (swarm) {
        for (var i=0; i<Ai.swarms.length; i++){
            if (swarm.id === Ai.swarms[i].id){
                Ai.states[i] = 'stay';
                Ai.targets[i] = null;
                Ai.targetTypes[i] = 'none';
            }
        }
    },
}
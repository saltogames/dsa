var animationIdAutoInc = 0;

var Animation = function (x, y, w, h, name, anim1FrameLength, frameCount, spriteUrl, isLooped) {    
    animationIdAutoInc++;
    this.id = animationIdAutoInc;    
    
    // CONSTANTS
    this.w = w;
    this.h = h;
    this.name = name;
    this.anim1FrameLength = anim1FrameLength       // ms
    this.frameCount = frameCount;                  // frames
    this.spriteUrl = spriteUrl;
    this.frameLengthInStep = Math.floor(this.anim1FrameLength / 1000 * world.framerate);
    this.isLooped = isLooped;
    this.isDead = false;
    
    // VARIABLES 
    this.x = x; 
    this.y = y;   
    this.s = 0;
          
    this.init = true;                 
};

// FUNCTIONS USED BY THE SCHEDULER  
Animation.prototype.step = function(){
    if (this.isDead) return;
    this.s++;                
    if (this.s > this.frameCount * this.frameLengthInStep){        
        // animation is over
        if (this.isLooped){
            this.s = 0;
            // HACK: this should be moved somehow to screen
            $("#animation_" + this.id).css("background-position-x", "0px");
            $("#animation_" + this.id).css("background-position-y", "0px");
        } else {
            this.delete();        
        }                
    } else {
        if (this.s % this.frameLengthInStep  === 0) {               
            // shift frame horisontally
            // HACK: this should be moved somehow to screen
            $("#animation_" + this.id).css(
                "background-position-x",
                (parseInt($("#animation_" + this.id).css("background-position-x")) - this.w) + "px"
            );
        }
        if (this.s % (this.frameLengthInStep * this.widthInFrames)  === 0) {               
            // shift frame vertically
            // HACK: this should be moved somehow to screen
            $("#animation_" + this.id).css(
                "background-position-y",
                (parseInt($("#animation_" + this.id).css("background-position-y")) - this.h) + "px"
            );
        }
        screen.draw(this);
    };  
};    

Animation.prototype.delete = function (){     
    delete world.animations[this.id];   
    screen.clear(this);
    this.isDead = true;
    
}


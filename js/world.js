world = {
    framerate : parameters.world.tickPerSec, // frame per sec
    w: parameters.world.width,
    h: parameters.world.height,
    swarms: [],
    playerSwarm: null, /*reference*/
    meteors: {},
    animations: {},
    flittingAnims: {},
    shipbubbles: {},
    //activeRrayGuns : {}, /*to be able to draw ray beams*/
    tick: 0,
    minimapScreenPercentage : 0.12,
    minimapScale : 0.028,
    cameraShakeIsOn: false,    
    isPaused : false,    
    shipsInSwarmsAtStart : 3,
    aiCount : 1,
    playerCount : 1,
    init : function (playerCount, aiCount, shipsInSwarmsAtStart, meteorCount){
        world.shipsInSwarmsAtStart = shipsInSwarmsAtStart;
        world.aiCount = aiCount;
        world.playerCount = playerCount;
        
        //world.ai = new Ai(new Swarm('red', aiSwarmShipCount));		
        world.advanced.addRandomMeteors(meteorCount);
    	//world.advanced.placeSwarm(swarmShipCount);

        // world.swarm.addRandomShips(count, 200, 200);
        // var cp = world.swarm.getCenter();
        // world.swarm.newTarget(cp.x, cp.y);

        // world.advanced.addShipBubble(30,30);     
        world.minimapScale = screen.w / world.w * world.minimapScreenPercentage;        
        screen.minimapJQ.width(world.w * world.minimapScale);
        screen.minimapJQ.height(world.h * world.minimapScale);
        screen.minimapBckJQ.width(world.w * world.minimapScale);
        screen.minimapBckJQ.height(world.h * world.minimapScale);
                
        world.advanced.colorPopper.init(parameters.world.aiCount + parameters.world.playerCount);                
        world.advanced.collision.init();
    },    
    start: function (t1id, t2id, t3id, color) {    	
    	for (var i = 0; i < world.aiCount; ++i) {              
            var p = null;                                    
            if (i === 0) {
                p = new Position(world.w-50, world.h-50);                
            } else if (i === 1) {
                p = new Position(50, world.h-50);
            } else if (i === 2) {
                p = new Position(world.w-50, 50);                
            } else {
                p = new Position(world.w / 2, world.h / 2);                
            }            
            var swarm = new Swarm('computer', world.advanced.colorPopper.pop(), world.shipsInSwarmsAtStart, p);
            world.swarms.push(swarm);
            Ai.add(swarm);
    	}    
        /*
    	for (var i = 0; i < 1; ++i) {
            //todofalu: megcsinalni, hogy tobbet is letre lehessen hozni
            var swarm = new Swarm('player', 'yellow', swarmShipCount, new Position(10, 10));
            world.swarms.push(swarm);
    	}
	*/        
    	var swarm = new Swarm('player', color, 0, new Position(10, 10));
    	swarm.addNewShip(t1id, 50, 50);
    	swarm.addNewShip(t2id, 100, 50);
    	swarm.addNewShip(t3id, 150, 50);
    	world.swarms.push(swarm);
        world.playerSwarm = swarm;


        // world.camera = {x: Math.floor(screen.w/2), y: Math.floor(screen.h/2)+300};
        world.camera = {x: 0, y: 0};
        
        // SCHEDULER
        setInterval(
            function () {                                          
                world.step();                          
            },
            (1000 / world.framerate)
        );

        // show minimap
        $("#minimap").css('display', 'block');
        $("#minimap_bck").css('display', 'block');
    },        
    step : function (){  
        // paused? 
        if (this.isPaused) return;              
                
        benchmark.onWorldStepStart();
                
        // the AI sets TARGETS ans STATES for all ai swarms        
        Ai.step();
        
        // ALL swarms steps/moves together
    	for(var i in this.swarms)
        {
            this.swarms[i].step();
        }
        
        for (var mid in world.meteors){             
            world.meteors[mid].step();
        }
        for (var aid in world.animations){                        
            world.animations[aid].step();
        }
        for (var faid in world.flittingAnims){                        
            world.flittingAnims[faid].step();
        }
        for (var sbid in world.shipbubbles){             
            world.shipbubbles[sbid].step();
        }
        //this.advanced.collision();        
        screen.drawBounds(); 
        var csx = 0;
        var csy = 0;
        if (world.cameraShakeIsOn){
            csx = Math.random() * 40 - 20;
            csy = Math.random() * 40 - 20;
        }
        // set camera for the active palyer        
        var activePlayerSwarm = this.getPlayers()[0];
        //var activePlayerSwarm = Ai.swarms[0];  // watch ai (debug)
        
        
        world.camera = {
            x: (world.camera.x * 15 + activePlayerSwarm.x) / 16 + csx,
            y: (world.camera.y * 15 + activePlayerSwarm.y) / 16 + csy,
        }
        
        screen.moveLattice();        
        screen.paralax.moveParalax();
        
        
        world.advanced.collision.step();                
        
        player.applyNewTarget();
        
        world.tick++;              
                
        
        benchmark.onWorldStepEnd();
    },
    getPlayers : function(){
        // todofalu : ezt memberbe kell kiszedni es nem minden alkalommal lekerni
    	var players = [];
    	for (let i in this.swarms) {
            if (this.swarms[i].owner == 'player') {
                players.push(this.swarms[i]);
            }
    	}
    	return players;
    },
    getComputers : function(){
    	// todofalu : ezt memberbe kell kiszedni es nem minden alkalommal lekerni
    	var computers = [];
    	for (let i in this.swarms) {
            if (this.swarms[i].owner == 'computer') {
                    computers.push(this.swarms[i]);
            }
    	}
    	return computers;
    },
    pause : function (){
        this.isPaused = true;
    },
    resume : function () {
        this.isPaused = false;
    },        
    doCameraShake : function (){                
        world.cameraShakeIsOn = true;
        setInterval(
            function () {                
                world.cameraShakeIsOn = false;
            },
            700
        );        
    },
    advanced : {
        placeSwarm : function(count){
            world.swarm.addRandomShips(count, 200, 200);
            var cp = world.swarm.getCenter();
            world.swarm.newTarget(cp.x, cp.y);
        },
        outOfBounds : function(x, y){
            return (
                (x > world.w) ||
                (x < 0)       ||
                (y > world.h) ||
                (y < 0)
            );
        },
        addRandomMeteors : function(c) {
            for (var i=0; i<c; i++){
                world.advanced.addMeteor(
                    Math.random() * (world.w - 600) + 300,
                    Math.random() * (world.h - 600) + 300);
            }   
        },
        addMeteor : function(x,y){
            var m = new Meteor(x,y);
            world.meteors[m.id] = m;
        },
        addExplosion : function(x,y){
            
            var a = new Animation(
                x,
                y,
                40,                                       // width of 1 frame
                40,                                       // height of 1 frame
                'explosion',                              // name
                40,                                       // delay of 1 frame
                10,                                       // width in frames
                //4                                     // height in frames
                'pics/sprites/small_explosion.png',       // url of the sprite
                false                                     // looped
            );
            world.animations[a.id] = a;                    
        },
        addBigExplosion : function(x,y){
            var a = new Animation(
                x,
                y,
                100,                                      // width of 1 frame
                100,                                      // height of 1 frame
                'explosion',                              // name
                40,                                       // delay of 1 frame
                10,                                       // width in frames
                //4,                                        // height in frames
                'pics/sprites/big_explosion.png',         // url of the sprite
                false                                     // looped
            );
            world.animations[a.id] = a;
        },
        addFlittingAnim : function(x,y, w, h, name, picURL){            
            var fa = new FlittingAnim(x, y, w, h, name, picURL);            
            world.flittingAnims[fa.id] = fa;
        },
        addShipBubble : function(x,y){
            var sb = new ShipBubble(x,y);
            world.shipbubbles[sb.id] = sb;
        },             	
        collision: {
            // collision detection using the quadtree js library
            qt : null, // quad tree
            init : function (){
                var bounds = new Rectangle(0,0, world.w, world.h);                
                this.qt = new QuadTree(bounds, false/*pointQuad*/, 7 /*maxdepth*/);
                
            },
            step : function(){                   
                // I) collect all collidable objects (they must have readable x,y and w fields)
                // TODO: manage this collection along with the object lifecicle, even better to make all collidable objects inherit from a base class                
                var objects = [];   
                var qt = null; // the quad tree
                for (var i=0; i<world.swarms.length; i++){                    
                    var swarm = world.swarms[i];
                    for (var sid in swarm.ships) {                               
                        objects.push({
                            type: 'ship',                            
                            data: swarm.ships[sid],
                            ownerSwarmIndex: i
                        });
                    }                    
                    for (var bid in swarm.bullets) {                        
                        objects.push({
                            type: 'bullet',
                            data: swarm.bullets[bid],
                            ownerSwarmIndex: i
                        });
                    }
                }  
                for (var mid in world.meteors) {
                    objects.push({
                        type: 'meteor',
                        data: world.meteors[mid],
                        ownerSwarmIndex: -1
                    });
                }                
                // console.log(objects.length);                                                
                    
                // II) apply quadtree for detect collision and call callback
                this.qt.clear();
                this.qt.insert(objects);
                for(var i = 0; i < objects.length; i++)
                {
                    var o = objects[i];
                    var items = this.qt.retrieve(o);
                    var len = items.length;
                    for(var j = 0; j < len; j++)
                    {                        
                        var item = items[j];
                        if(o == item) continue; // no collision with same                                                        
                        var dx = o.data.x - item.data.x;
                        var dy = o.data.y - item.data.y;      
                        var r = o.data.w/2 + item.data.w/2;		
                       

                        var colliding = (( dx * dx )  + ( dy * dy )) < (r * r);
                        if (colliding){
                            this.detectedCallback(o, item);                            
                        }                               
                    }
                }
                
                // ... 
                
            },
            detectedCallback : function(objA, objB){                          
                
                // shooting meteors
                if ((objA.type == 'bullet')){                    
                    if ((objB.type == 'meteor')){                    
                        // bullet -> meteor
                        world.advanced.addExplosion(objA.data.x, objA.data.y);
                        objA.data.delete(); // delete the bullet                         
                        objB.data.health -= objA.data.damage;                        
                    }
                    if ((objB.type == 'ship') && (objA.ownerSwarmIndex != objB.ownerSwarmIndex)){                    
                        // bullet -> other swarms ship
                        world.advanced.addExplosion(objA.data.x, objA.data.y);
                        objA.data.delete(); // delete the bullet 
                        objB.data.health -= objA.data.damage;
                    }                    
                }
                if ((objA.type == 'ship') && (objB.type == 'ship' || objB.type == 'meteor') && (objA.ownerSwarmIndex != objB.ownerSwarmIndex)){  
                    if (objA.data.health >=objB.data.health){
                        world.doCameraShake();
                        screen.doScreenFlash('redctc ttio d');
                        world.advanced.addBigExplosion(objA.data.x, objA.data.y);
                        objA.data.health -= objB.data.health;
                        objB.data.health = 0;                        
                    } else {
                        world.doCameraShake();
                        screen.doScreenFlash('red');
                        world.advanced.addBigExplosion(objA.data.x, objA.data.y);
                        objB.data.health -= objA.data.health;
                        objA.data.health = 0;                        
                    }                                       
                }
                
                
                // what to do if two objects collide
                // e.g. if two ships within the same swarm: nothing                
                
            },
        }
        ,
        colorPopper : {            
            colors : null,
            shuffle : function() {                
                if (this.colors == null) return;
                var j, x, i;
                for (i = this.colors.length; i; i -= 1) {
                    j = Math.floor(Math.random() * i);
                    x = this.colors[i-1];
                    this.colors[i-1] = this.colors[j];
                    this.colors[j] = x;
                }
            },
            init : function(colorCount){
                var c = ['yellow', 'green', 'red', 'purple', 'red', 'red', 'red', 'red', 'red', 'red', 'red', 'red', 'red'];
                this.colors = c.slice(0, colorCount);                
                this.shuffle();
            },
            pop : function(){
                return this.colors.pop();
            }                                            
        }
    },
    camera : {
        x: -1,
        y: -1
    },
    getCameraOffset : function(){
        return {
            x : -world.camera.x + screen.w / 2,
            y : +world.camera.y - screen.h / 2
        }
    }
    
}
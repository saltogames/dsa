var bulletIdAutoInc = 0;

var Bullet = function (s, bulletParameters, angle) {    
    bulletIdAutoInc++;
    this.id = bulletIdAutoInc;      
    this.bp = bulletParameters;
    
    // CONSTANTS
    this.max_speed = bulletParameters.maxSpeed;
    this.damage = bulletParameters.damage;        
    this.w = bulletParameters.w; 
    this.h = bulletParameters.h;
    
    // VARIABLES 
    this.x = s.x;
    this.y = s.y;
    this.angle = angle;
    this.headingAngle = angle;
    this.speed = this.max_speed;     
    this.ship = s;
    this.startX = s.x;
    this.startY = s.y;
};

// FUNCTIONS USED BY THE SCHEDULER        
Bullet.prototype.step = function(){             
    var d = geoMath.distance(this.x,this.y,this.startX,this.startY);
    if (d > this.bp.maxTravelDistance) {
        this.delete();
    } else {
        this.float();
        if (this.ship.typeId === 3 /*Hammer*/){
           this.angle -= (1 / this.bp.turnPeriod) * 360 / world.framerate;         // rotate 
        }
        screen.draw(this);
    }
};

Bullet.prototype.float = function (){
    if (this.speed > 0) {        
        // var v = geoMath.polarToVector(this.speed / world.framerate, angle);     circle lol
        var v = geoMath.polarToVector(this.speed / world.framerate, this.headingAngle);
        this.x += v.x;
        this.y += v.y;
    }
};

Bullet.prototype.delete = function (){    
    screen.clear(this);
    delete this.ship.swarm.bullets[this.id]; 
}
/*Bullet.prototype.rotate = function (){                    
    this.angle += (1 / this.turnPeriod) * 360 / world.framerate;        
};*/


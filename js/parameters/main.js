Position = function (x, y) {
    this.x = x;
    this.y = y;
}

distance = function (a, b) {
    //todofalu : object existance checker
    return geoMath.distance(a.x, a.y, b.x, b.y);
}

parameters = {};

parameters.ship = {
    w: 32, // px
    h: 32, // px    
}

/*original for 3`tick per sec*/
/*parameters.swarm = {
    tooCloseTreshold : 60,                          // px
    tooCloseScale : 0.7,                            // %
    tooFarTreshold : 147,                           // px
    tooFarScale : 0.63,                             // %
    avgAngleScale : 0.001,                          // %      
    takeActionEasing : 0.035, 
    swarmBasicSightRadius : 1500,
}*/

parameters.swarm = {
    tooCloseTreshold : 65,                          // px
    tooCloseScale : 2,                            // %
    tooFarTreshold : 187,                           // px
    tooFarScale : 2,                             // %
    avgAngleScale : 0.001,                          // %      
    takeActionEasing : 0.08, 
    swarmBasicSightRadius : 1500,
    maxShipCount: 8,
}

parameters.meteor = {    
    // WORLD
    health: 1200,    
    turnPeriod: 30,                                 // sec    
    // DRAW
    width: 66,                                      // px
    height: 63,                                     // px
    color: 'gray',
};

parameters.world = {    
    width: 5111,                                    // px
    height: 5111,                                   // px
    tickPerSec: 60,
    playerCount : 1,    
    aiCount : 3,    
    shipsInSwarmsAtStart: 4,                        // for those AIs
    meteorCount: 31,
    swarmSightRange: 1200,                          // px
    shipBubbleX: 333,
    shipBubbleY: 333,	
};

parameters.shipBubble = {    
    width: 63,                                     // px
    height: 63,
    turnPeriod: 5,                                 // sec 
    changePeriod: 1.3,                             // sec 
};

parameters.ai= {    
    triggerRange : 20,
    attackDistance : 520,                         // px
    escapeDistance : 150,                         // px 
    meteorEvadeDistance: 250   
};

parameters.explosionParameters = {
    // DRAW
    width: 66,                                    // px
    height: 66                                    // px
}

parameters.paralax = {
    objw : 977,                                   // px
    objh: 687,                                    // px
    layer1ObjCount: 4, 
    layer2ObjCount: 7,
    layer1Scale: 31,
    layer2Scale: 11,
};

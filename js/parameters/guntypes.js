parameters.gunNames = ['JuniorLaserGun', 'QuickLaserGun', 'DoubleLaserGun', 'GravityGun', 'SimpleMinerRayGun'];
parameters.gunTypes = {
    'JuniorLaserGun' : {
        id: 1,
        damageTransmitter: 'bullet',
        cooldown: 1, // sec        
        bullet: {
            damage: 65, // hp
            damageType: 'laser',
            maxSpeed: 750, // px / sec
            maxTravelDistance: 400, // px
            w: 5, // px
            h: 18,
            url: 'pics/ships/bullet_simple.png'
        }
    },
    'QuickLaserGun' : {
        id: 2,
        damageTransmitter: 'bullet',
        cooldown: 0.5,                               // sec        
        bullet : {
            damage: 65,                
            damageType: 'laser',                     // hp
            maxSpeed: 920,                           // px / sec
            maxTravelDistance: 864,                  // px
            w: 5,                                    // px
            h: 18,   
            url: 'pics/ships/bullet_stronger.png'
        }
    },
    'DoubleLaserGun' : {
        id: 3,
        damageTransmitter: 'bullet',
        cooldown: 1,                                    // sec        
        bullet : {
            damage: 105,                                // hp
            damageType: 'laser',
            maxSpeed: 750,                              // px / sec
            maxTravelDistance: 1100,                    // px
            w: 34,                                      // px
            h: 35,   
            url: 'pics/ships/bullet_heavy.png'
        }
    },
    'GravityGun' : {
        id: 4,
        damageTransmitter: 'bullet',
        cooldown: 1.3,                              // sec        
        bullet : {
            damage: 155, // hp
            damageType: 'gravity',
            maxSpeed: 550, // px / sec
            maxTravelDistance: 1300, // px
            w: 32, // px
            h: 32,
            url: 'pics/ships/bullet_gravity.png',
            turnPeriod: 1, // sec
        }
    },
    'SimpleMinerRayGun' : {
        id: 5,
        damageTransmitter: 'ray',                 
        ray : {
            miner: true,
            maxTargetDistance: 500, // px      
            dps: 28,  // damage per second (hp) - double to meteor                       
            damageType: 'laser',
            color: 'blue'
        }
    }
}
parameters.shipNames = ['Valkyr', 'Hydra', 'Trobadur', 'Hammer', 'Raptor', 'Angel' ];
parameters.shipTypes = {
    'Valkyr' : {
        id: 0,
        name: 'Valkyr',          
        health: 230,                                    // hp
        maxSpeed: 300/*+150*/,                                  // px / sec                 
        w: 32, // px
        h: 32,
        gun: parameters.gunTypes['QuickLaserGun'],
        //desc: 'As a swift laser fighter he bursts damage then he heads back until recharge. He is picky on calling him shy. <p><i>"Valkyr drivers traditionally change frags into beers after eight."</i></p>', 
        desc: 'TEMP: the guy with a QuickLaser gun <p><i>"Later we will make a mighty Valkyr with his own personality. I promise. Valkyr is my personal favourite."</i></p>',       
        stats: [1,1,2,2,2,0] // health, damage, fireRate, fireRange, mobility, support            
    },
    'Hydra' : {
        id: 1,
        health: 280, // hp
        maxSpeed: 300/*+150*/, // px / sec 
        name: 'Hydra',   
        w: 32, // px
        h: 32,
        //desc: 'Electricity specialist. Can hit multiple enemies directly by electrycity within short range. Recharge rate is slow. <p><i>"Bitches like lightnings."</i></p>',
        desc: 'TEMP: the guy with a DoubleLaser gun <p><i>"Later we will make a mighty Hydra with his own personality. I promise more."</i></p>',       
        gun: parameters.gunTypes['DoubleLaserGun'],
        stats: [2,2,1,3,2,0] // health, damage, fireRate, fireRange, mobility, support  
    },
    'Trobadur': {
        id: 2,
        health: 220,                                    // hp
        maxSpeed: 300/*+150*/,                                  // px / sec 
        name: 'Trobadur',
        w: 32, // px
        h: 32,
        desc: 'Having a Trobadour in army is all about having a better sight. Whereas its combat abilites are low, it boosts the sight range of the radar, and also makes ALL meteors visible on map as long as it is alive. Sight boosts from multiple Trobadours are cummulative.',
        gun: parameters.gunTypes['JuniorLaserGun'],        
        stats: [1,1,1,2,2,2], // health, damage, fireRate, fireRange, mobility, support  
        special : {
            sightRadiusModification: 200,           
        }, 
    },
    'Hammer' : {
        id: 3,
        health: 190, // hp
        maxSpeed: 300/*+150*/, // px / sec 
        name: 'Hammer',
        w: 32, // px
        h: 32, // px    
        url_prefix: 'pics/ships/ship3',
        //desc: 'Fires a gravity bomb that heads the centerpoint of the target swarm. Yes. Its AOE. And its fun watching how they are scrumbled.',
        desc: 'TEMP: Fires a gravity bomb that heads the centerpoint of the target swarm. Yes. Its AOE. And its fun watching how they are scrumbled. <p><i>"Gravity bomb is to be improved. It just big damage now. Sorry am I."</i></p>',        
        gun: parameters.gunTypes['GravityGun'],
        stats: [2,3,1,3,2,0] // health, damage, fireRate, fireRange, mobility, support
    },
    'Raptor' : {
        id: 4,
        health: 320, // hp
        maxSpeed: 300/*+150*/, // px / sec 
        name: 'Raptor',
        w: 32, // px
        h: 32, // px                
        desc: 'Combat miner. Focuses the most healty target and does damage over time effect with a laser beam. Prefers to focus enemy ships over meteors. 2x damage to meteors. <p><i>"Rather spikey beam for a miner aye?"</i></p>',                                            
        gun: parameters.gunTypes['SimpleMinerRayGun'],                 
        stats: [3,1,3,2,2,1] // health, damage, fireRate, fireRange, mobility, support
    },
    'Angel' : {
        id: 5,
        health: 230, // hp
        maxSpeed: 300/*+150*/, // px / sec 
        name: 'Angel',
        w: 32, // px
        h: 32, // px                
        desc: 'Heals the most injured ship periodically. No attack ability. <p><i>"They will help you and they will speak of their religion no matter what the circumstances are."</i></p>',
        gun: null,                            
        stats: [2,0,0,0,2,3], // health, damage, fireRate, fireRange, mobility, support
        special : {
            healCooldown: 1.3,
            healAmount: 25,
        },    
    },
    getById : function(id){
        for (var i=0; i<parameters.shipNames.length; i++){
            var st = parameters.shipTypes[parameters.shipNames[i]];
            if (st.id == id) return st;
        }
    }
}    

